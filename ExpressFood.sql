DROP DATABASE IF EXISTS ExpressFood;
CREATE DATABASE ExpressFood;
USE ExpressFood;


/*========CREATION DES TABLES=========*/


CREATE TABLE CLIENTS (
	Id_client 					int AUTO_INCREMENT,
    Nom_client 					varchar(50),
    Prenom_client 				varchar(50),
    Email_client 				varchar(50) UNIQUE,
    Tel_client 					int(10) UNIQUE,
    
    Id_adresse					int,
    
    PRIMARY KEY 				(Id_client),
    
    FULLTEXT INDEX ind_Nom_client (Nom_client),
    FULLTEXT INDEX ind_Prenom_client (Prenom_client)
);

CREATE TABLE ADRESSE (
	Id_adresse 					int AUTO_INCREMENT,
    Num_adresse 				int unsigned,
    Rue 						varchar(255),
    Ville 						varchar(255),
    
    Id_client					int,
    
    PRIMARY KEY 				(Id_adresse),
    
    FULLTEXT INDEX ind_Ville (Ville)
);

CREATE TABLE PRODUIT(
	Id_produit 					int AUTO_INCREMENT,
    Nom_produit 				varchar(100),
    Type_produit 				varchar(50),
    Prix_produit 				double,
    Date_produit 				date,
    
    PRIMARY KEY 				(Id_produit),
    
    FULLTEXT INDEX ind_Nom_produit (Nom_produit),
    FULLTEXT INDEX ind_Type_produit (Type_produit)
);

CREATE TABLE PANIER (
	Id_panier 					int,
    Id_client					int
    

);

CREATE TABLE PRODUIT_PANIER (
	Id_produit					int NOT NULL,
    Id_panier					int NOT NULL,
    
    PRIMARY KEY					(Id_produit, Id_panier)
);

CREATE TABLE CLIENT_PANIER (
	Id_client					int NOT NULL,
    Id_panier					int NOT NULL,
    
    PRIMARY KEY					(Id_client, Id_panier)
);

CREATE TABLE COMMANDE(
	Id_commande 				int AUTO_INCREMENT,
    Statut_commande 			varchar(50),
    Prix_commande 				double,
    Heure_commande 				time,
    Date_commande 				date,
    
    Id_client					int,
    Id_produit					int,
    Id_panier					int,
    
    PRIMARY KEY 				(Id_commande)
);

CREATE TABLE LIVREUR(
	Id_livreur 					int AUTO_INCREMENT,
    Nom_livreur 				varchar(50), 
    Position_livreur 			varchar(100),
    Temps_livraison 			int(50),
    
    Id_statut					int,
    Id_commande					int,
    
    PRIMARY KEY 				(Id_livreur),
    
    FULLTEXT INDEX ind_Nom_livreur (Nom_livreur)
);

CREATE TABLE STOCK(
	Id_stock					int AUTO_INCREMENT,
    Quantite_produit 			int(255),
    
    Id_produit					int,
    
    PRIMARY KEY 				(Id_stock),
    
    INDEX ind_Quantite_produit (Quantite_produit)
);

CREATE TABLE STOCK_LIVREUR(
    Quantite_produit_livreur 	int(10),
    
	Id_produit					int,
    Id_livreur					int,
    
    PRIMARY KEY					(Id_produit, Id_livreur)
);

CREATE TABLE STATUT_LIVREUR(
	Id_statut 					int AUTO_INCREMENT,
    Type_statut 				varchar(50),
    
    PRIMARY KEY 				(Id_statut)
);


/*========AJOUT DES CLES ETRANGERES========*/


ALTER TABLE CLIENTS
ADD CONSTRAINT fk_clients_adresse FOREIGN KEY (Id_adresse) REFERENCES ADRESSE(Id_adresse);

ALTER TABLE ADRESSE
ADD CONSTRAINT fk_adresse_clients FOREIGN KEY (Id_client) REFERENCES CLIENTS(Id_client);

ALTER TABLE PANIER
ADD CONSTRAINT fk_panier_client FOREIGN KEY (Id_client) REFERENCES CLIENTS(Id_client);

ALTER TABLE PRODUIT_PANIER
ADD CONSTRAINT fk_prodpanier_produit FOREIGN KEY (Id_produit) REFERENCES PRODUIT(Id_produit),
ADD CONSTRAINT fk_prodpanier_panier FOREIGN KEY (Id_panier) REFERENCES PANIER(Id_panier);

ALTER TABLE CLIENT_PANIER
ADD CONSTRAINT fk_cltpanier_panier FOREIGN KEY (Id_panier) REFERENCES PANIER(Id_panier),
ADD CONSTRAINT fk_cltpanier_client FOREIGN KEY (Id_client) REFERENCES CLIENTS(Id_client);

ALTER TABLE COMMANDE
ADD CONSTRAINT fk_commande_client FOREIGN KEY (Id_client) REFERENCES CLIENTS(Id_client),
ADD CONSTRAINT fk_commande_produit FOREIGN KEY (Id_produit) REFERENCES PRODUIT(Id_produit),
ADD CONSTRAINT fk_commande_panier FOREIGN KEY (Id_panier) REFERENCES PANIER(Id_panier);

ALTER TABLE LIVREUR
ADD CONSTRAINT fk_livreur_statut FOREIGN KEY (Id_statut) REFERENCES STATUT_LIVREUR(Id_statut),
ADD CONSTRAINT fk_livreur_commande FOREIGN KEY (Id_commande) REFERENCES COMMANDE(Id_commande);

ALTER TABLE STOCK
ADD CONSTRAINT fk_stock_produit FOREIGN KEY (Id_produit) REFERENCES PRODUIT(Id_produit);

ALTER TABLE STOCK_LIVREUR
ADD CONSTRAINT fk_stklivreur_produit FOREIGN KEY (Id_produit) REFERENCES PRODUIT(Id_produit),
ADD CONSTRAINT fk_stklivreur_livreur FOREIGN KEY (Id_livreur) REFERENCES LIVREUR(Id_livreur);


/*=======DONNEES DE TEST========*/


INSERT INTO CLIENTS (Nom_client, Prenom_client, Email_client, Tel_client, Id_adresse)
VALUES 	('Doe', 'John', 'johndoe@gmail.com', 0681562357, 1),
		('Dark', 'Vador', 'darkvador@gmail.com', 0600652323, 2),
        ('Obi-wan', 'Kenobi', 'obiwankenobi@gmail.com', 0650009875, 3);

INSERT INTO ADRESSE (Num_adresse, Rue, Ville, Id_client)
VALUES 	(241, 'Rue de la gare', 'Metz', 1),
		(24, 'Rue de l\'église', 'Metz', 2),
        (150, 'Rue de la gare', 'Metz', 3);

INSERT INTO PRODUIT (Nom_produit, Type_produit, Prix_produit, Date_produit)
VALUES 	('Salade', 'Entree', 8, '2017-11-22'),
		('Nems', 'Plat', 15, '2017-11-22'),
		('Glace', 'Dessert', 12, '2017-11-22');
        
INSERT INTO PANIER (Id_panier, Id_client)
VALUES	(1, 1),
		(2, 2),
        (2, 2);

INSERT INTO PRODUIT_PANIER (Id_produit, Id_panier)
VALUES	(2, 1),
		(1, 2),
		(3, 2);
        
INSERT INTO CLIENT_PANIER(Id_client, Id_panier)
VALUES	(1, 1),
		(2, 2),
        (1, 2);

INSERT INTO COMMANDE (Statut_commande, Prix_commande, Heure_commande, Date_commande, Id_client, Id_panier)
VALUES	('Valider', 30, '20:25:35', '2017-11-22', 1, 1),
		('Valider', 20, '20:35:56', '2017-11-22', 2, 2),
        ('En cours d\'acheminement', 12, '20:45:12', '2017-11-22', 3, 3);

INSERT INTO LIVREUR (Nom_livreur, Position_livreur, Temps_livraison, Id_statut, Id_commande)
VALUES	('Damien', 'Dêpot', 0, 1, 0),
		('David', 'Dêpot', 0, 3, 0),
		('Vincent', 'Rue de l\'église', 10, 2, 1);
	

INSERT INTO STOCK (Quantite_produit, Id_produit)
VALUES	(200, 1),
		(147, 2);
        
INSERT INTO STOCK_LIVREUR (Quantite_produit_livreur, Id_produit, Id_livreur)
VALUES	(10, 1, 1),
		(7, 2, 1);
        
INSERT INTO STATUT_LIVREUR (Id_Statut, Type_statut)
VALUES	(1, 'Libre'),
		(2, 'En livraison'),
        (3, 'Indisponible');
        